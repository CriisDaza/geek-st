<?php

class ProductoDAO
{

    private $idproducto;
    private $nombre;
    private $precio;
    private $cantidad;
    private $imagen;
    private $proveedor_idproveedor;
    private $marca_idmarca;
    private $tipoproducto_idtipoproducto;

    public function ProductoDAO($id = "", $nombre = "", $precio = "", $cantidad = "", $imagen = "", $proveedor = "", $marca = "", $tipoProducto = "")
    {
        $this->idproducto = $id;
        $this->nombre = $nombre;
        $this->precio = $precio;
        $this->cantidad = $cantidad;
        $this->imagen = $imagen;
        $this->proveedor_idproveedor = $proveedor;
        $this->marca_idmarca = $marca;
        $this->tipoproducto_idtipoproducto = $tipoProducto;
        
    }

    public function Crear()
    {
       return "insert into producto (nombre, precio, cantidad, proveedor_idproveedor, marca_idmarca, tipoproducto_idtipoproducto)
              values (
              '" . $this->nombre . "',
              '" . $this->precio . "',
              '" . $this->cantidad . "',
              '" . $this->proveedor_idproveedor . "',
              '" . $this->marca_idmarca . "',
              '" . $this->tipoproducto_idtipoproducto . "'
              )";
    }
    
    public function Consultar(){
        
        return "select idproducto, nombre, precio, cantidad, imagen, proveedor_idproveedor, marca_idmarca, tipoproducto_idtipoproducto
                from producto where idproducto = '" . $this ->idproducto . "'"; 
    }
    
    public function ConsultarTodos($atributo, $direccion, $filas , $pag){
        
        return "select idproducto, nombre, precio, cantidad, imagen, proveedor_idproveedor, marca_idmarca, tipoproducto_idtipoproducto
               from producto " . 
               (($atributo != "" && $direccion != "")?"order by ". $atributo . " " . $direccion:"") .
               " limit " . (($pag-1)*$filas) . ", " . $filas;
    }
    
    public function ConsultarVendidosvsinv(){
        return "select p.nombre,(p.cantidad) as inventario, sum(pf.unidades) as cantidad
                from pfactura pf left join producto p on (pf.producto_idproducto = p.idproducto  )
                group by p.nombre
                order by cantidad DESC";
    }
    
    public function ConsultarVendidos(){
        return "select p.nombre, sum(pf.unidades) as cantidad
                from pfactura pf left join producto p on (pf.producto_idproducto = p.idproducto  )
                group by p.nombre
                order by cantidad DESC";
    }
    
    public function ConsultarTotalFilas(){
        
        return "select count(idproducto)
                from producto";
    }
    
    public function ConsultarFiltro($filtro){
        
        return "select idproducto, nombre, precio, cantidad, imagen, proveedor_idproveedor, marca_idmarca, tipoproducto_idtipoproducto
                from producto
                where nombre like '" . $filtro . "%'
                order by nombre DESC" ;
    }
    
    public function EditarImagen(){
        
        return "update producto set imagen = '" . $this -> imagen . "'
                where idproducto = '" . $this -> idproducto . "'";
    }
    
    public function Agregar() {
        
        return "UPDATE producto SET cantidad = producto.cantidad + '" . $this -> cantidad . "'
               WHERE idproducto='" . $this -> idproducto . "'";
    }
    public function ModPrecio() {
        
        return "UPDATE producto SET precio ='" . $this -> precio . "'
               WHERE idproducto='" . $this -> idproducto . "'";
    }
    public function consultarTodosReporte(){
        return "select idproducto, nombre, precio, cantidad, imagen, proveedor_idproveedor, marca_idmarca, tipoproducto_idtipoproducto
                from producto
                order by nombre";
    }
    public function consultarProductosPorMarca(){
        return "select m.nombre as marca, count(p.idproducto) as cantidad
                from marca m left join producto p on (m.idmarca = p.marca_idmarca)
                group by m.nombre
                order by m.nombre asc";
    }
    
    public function consultarProductosPorTipo(){
        return "select t.nombre, count(p.idproducto) as cantidad
                from tipoproducto t left join producto p on (t.idtipoproducto = p.tipoproducto_idtipoproducto)
                group by t.nombre
                order by t.nombre asc";
    }
    
    public function consultarProductosPorProveedor(){
        return "select pr.nombre, count(p.idproducto) as cantidad
                from proveedor pr left join producto p on (pr.idproveedor = p.proveedor_idproveedor)
                group by pr.nombre
                order by pr.nombre asc";
    }
    
    public function consultarProductosPorPrecio(){
        return "(select '0 - 50k', count(idproducto)
                from producto
                where precio <= 50000)
                union
                (select '50k - 100k', count(idproducto)
                from producto
                where precio > 50000 and precio <= 100000)
                union
                (select '100k - 500k', count(idproducto)
                from producto
                where precio > 100000 and precio <= 500000)
                union
                (select '500k - 1M', count(idproducto)
                from producto
                where precio > 500000 and precio <= 1000000)
                union
                (select '1M+', count(idproducto)
                from producto
                where precio > 1000000)";
    }
}


