<?php
include "presentacion/menuAdmin.php";
?>
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Buscar Producto</h5>
				<div class="card-body">
					<div class="row">
						<div class="col-4"></div>
						<div class="col-4">
							<input type="text" class="form-control" id="filtro">
						</div>
					</div>					
					<div id="resultados" ></div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$("#filtro").keyup(function() {
	var filtro = $("#filtro").val();
	if(filtro.length >= 3){		
		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/producto/buscarProductoAjax.php")?>&filtro=" + filtro;	
		$("#resultados").load(url);		
	}else{
		$("#resultados").html("")
	}
});
</script>