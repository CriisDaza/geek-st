<?php
require_once "logica/Producto.php";

$producto = new Producto();
$productos = $producto->consultarFiltro($_GET["filtro"]);

if($productos!=null){
?>
<div class="container">
<table class="table table-dark table-striped mt-3">
					<thead >
					<tr >
					<th  >#</th>
					<th >Nombre
					<th >Precio
					</th >
								<th >Cantidad</th>
								<th >Imagen</th>
								<th >Marca</th>
								<th >Tipo</th>
								</tr>
								</thead>
								<tbody>
								<?php 
								$pos=1;
								foreach ($productos as $productoActual){
								    echo "<tr>";
								    echo "<td>" . $pos ++ . "</td>
                                      <td >" . $productoActual ->getNombre() . "</td>
                                      <td>" . $productoActual -> getPrecio() . "</td>
                                      <td>" . $productoActual -> getCantidad() . "</td>
                                      <td>" . (($productoActual -> getImagen()!="")?"<img src='" . $productoActual -> getImagen() . "' height='40px' />":"") . "</td>
                                      <td>" . $productoActual -> getMarca() -> getNombre() . "</td>
                                      <td>" . $productoActual -> getTipoproducto() -> getNombre() . "</td>";
								    echo "</tr>";
								}
								?>
								</tbody>
					</table>
					
					                                
		<?php }else{ ?>
					<div class="alert alert-danger">No se encontraron resultados</div>

<?php }?>
					</div>
					