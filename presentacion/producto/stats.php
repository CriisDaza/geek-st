<?php

$producto = new Producto();
$productosPorMarca = $producto -> consultarProductosPorMarca();
$productosPorTipo = $producto -> consultarProductosPorTipo();
$productosPorPrecio = $producto -> consultarProductosPorPrecio();
$productosporProveedor= $producto -> consultarProductosPorProveedor();
$productosvvsinv= $producto -> ConsultarVendidosvsinv();
$productosvendidos=$producto ->ConsultarVendidos();

?>

<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Estadisticas</h5>
				<div class="card-body ">
				<div id="productosvendidos" style="width: 1000px; height: 300px;"></div>
				<div id="productosvs" style="width: 1000px; height: 300px;"></div>
					<div id="productosPorMarca" style="width: 1000px; height: 300px;"></div>
					<div id="productosPorTipo" style="width: 1000px; height: 300px;"></div>
					<div id="productosPorPrecio" style="width: 1000px; height: 300px;"></div>
					<div id="productosPorProveedor" style="width: 1000px; height: 300px;"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
google.charts.load("current", {packages:['corechart','bar']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
    var dataMarca = google.visualization.arrayToDataTable([
        ["Marca", "Cantidad"],
        <?php 
        foreach ($productosPorMarca as $p){
            echo "['" . $p[0] . "', " . $p[1] . "],\n";        
        }        
        ?>
    ]);
    
    var viewMarca = new google.visualization.DataView(dataMarca);
    
    var optionsMarca = {
        title: "Productos por Marca",
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
    };
    var chartMarca = new google.visualization.ColumnChart(document.getElementById("productosPorMarca"));
    chartMarca.draw(viewMarca, optionsMarca);

    
    var dataTipo = google.visualization.arrayToDataTable([
        ["Marca", "Cantidad"],
        <?php 
        foreach ($productosPorTipo as $p){
            echo "['" . $p[0] . "', " . $p[1] . "],\n";        
        }        
        ?>
    ]);
    
    var viewTipo = new google.visualization.DataView(dataTipo);
    
    var optionsTipo = {
        title: "Productos por Tipo",
        bar: {groupWidth: "95%"},
        is3D: true,
        legend: { position: "right" },
    };
    
    var chartTipo = new google.visualization.PieChart(document.getElementById("productosPorTipo"));
    chartTipo.draw(viewTipo, optionsTipo);

    var dataProv = google.visualization.arrayToDataTable([
        ["Proveedor", "Cantidad"],
        <?php 
        foreach ($productosporProveedor as $p){
            echo "['" . $p[0] . "', " . $p[1] . "],\n";        
        }        
        ?>
    ]);
    
    var viewProv = new google.visualization.DataView(dataProv);
    
    var optionsProv = {
        title: "Productos por Proveedor",
        bar: {groupWidth: "95%"},
        is3D: true,
        legend: { position: "right" },
    };
    var chartProv = new google.visualization.PieChart(document.getElementById("productosPorProveedor"));
    chartProv.draw(viewProv, optionsProv);
    
    var dataPrecio = google.visualization.arrayToDataTable([
        ["Marca", "Cantidad"],
        <?php 
        foreach ($productosPorPrecio as $p){
            echo "['" . $p[0] . "', " . $p[1] . "],\n";        
        }        
        ?>
    ]);
    
    var viewPrecio = new google.visualization.DataView(dataPrecio);
    
    var optionsPrecio = {
        title: "Productos por Rango de Precio",
        bar: {groupWidth: "95%"},
        is3D: true,
        legend: { position: "right" },
    };
    var chartPrecio = new google.visualization.PieChart(document.getElementById("productosPorPrecio"));
    chartPrecio.draw(viewPrecio, optionsPrecio);

    var dataVs = google.visualization.arrayToDataTable([
        ['Nombre', 'Inventario', 'Vendidos'],
        <?php 
                foreach ($productosvvsinv as $p)
                    echo "['" . $p[0] . "', " . $p[1] . ", " . $p[2] ."],\n";        
               
               ?>
      ]);

      var optionsVs = {
        chart: {
        	 title: "Productos vendidos y en inventario",
             legend: { position: "center" },
          
        }
      };

      var chartVs = new google.charts.Bar(document.getElementById("productosvs"));

      chartVs.draw(dataVs,(optionsVs));

      var data = google.visualization.arrayToDataTable([
    	  ['Nombre', 'unidades vendidas'],
          <?php 
                  foreach ($productosvendidos as $p)
                      echo "['" . $p[0] . "', " . $p[1] ."],\n";        
                 
                 ?>
        ]);

        var options = {
          title: 'Productos vendidos',
          legend: 'none',
          pieSliceText: 'label',
          slices: {  4: {offset: 0.2},
                    12: {offset: 0.3},
                    14: {offset: 0.4},
                    15: {offset: 0.5},
          },
        };

        var chart = new google.visualization.PieChart(document.getElementById('productosvendidos'));
        chart.draw(data, options);

}
</script>