<?php

require_once "logica/Producto.php";
include "presentacion/menuInicio.php";

$producto = new Producto();
$productos = $producto->consultarFiltro($_POST["filtro"]);
if($productos!=null){
?>

<div class="container">
					<br>
						<div class="row">
								<?php
								
                                foreach ($productos as $productoActual) {
                                    ?>
                                 
                                        <div class="col-3">
                                          <div class="card" style="width: 18rem;">
                                          <?php echo "<a href='modalProducto.php?id=" . $productoActual->getId() . "' data-bs-toggle='modal' data-bs-target='#modalProducto'>" ?>
                                          <img
                                           
                                           class="card-img-top"
                                           src= <?php echo $productoActual->getImagen()?>
                                           >
                                           <?php echo "</a>"?>
                                           <div class="card-body">
                                           
                                           <span>
                                           <?php $productoActual->getNombre() ?> </span>
                                           <h3 class="card-titule"> <?php echo $productoActual->getPrecio(); ?> </h3>
                                           <p class="card-text"> <?php echo $productoActual->getNombre(); ?> </p>
                                        
                                           </div>
                                          </div>
                                           <br>
                                          </div>
                                     
                                    <?php } ?>
                                      </div> 
		<?php }else{ ?>
					<div class="alert alert-danger">No se encontraron resultados</div>

<?php }?>
</div>					


<div class="modal fade" id="modalProducto" tabindex="-1"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
</div>

<script>
	$('body').on('show.bs.modal', '.modal', function (e) {	    
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>
