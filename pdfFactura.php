<?php
require_once 'fpdf/fpdf.php';
require_once 'logica/Producto.php';
require_once 'logica/Proveedor.php';
require_once 'logica/Factura.php';
require_once 'logica/TipoProducto.php';
require_once 'logica/Marca.php';
if(isset($_POST['putito'])){
    
$fac = new Factura($_POST['fac']);
$productos = $fac->ConsultarProductos();
$res=$fac->Consultar();
}
$pdf = new FPDF("P","mm","LETTER");
$pdf -> AddPage();

$pdf->SetFont("Times","B",16);
$pdf->Cell(196,10,"Geek Store",0,1,"C");
$pdf->Cell(196,10,"Factura". " ".$_POST['fac'],0,1,"C");

$pdf->SetFont("Times","B",13);
$pdf->Cell(196,10,"FECHA: ".$res[0],0,1,"L");
$pdf->Cell(196,10,"HORA: ".$res[1],0,1,"L");
$pdf->Cell(196,10,"CLIENTE: ".$res[3]." ".$res[4],0,1,"L");
$pdf->Cell(196,10," ",0,1,"L");

$pdf->SetFont("Times","B",12);
$pdf->Cell(83,8,"Descripcion",0,0,"C");
$pdf->Cell(20,8,"cantidad",0,0,"C");
$pdf->Cell(49,8,"precio",0,0,"C");
$pdf->Cell(49,8,"subtotal",0,1,"C");
$pdf->Cell(196,10," ",0,1,"L");


$pdf->SetFont("Times","",12);
foreach ($productos as $productoActual) {
    $alto = 8;
    if(strlen($productoActual -> getNombre()) <= 33){
        $pdf->Cell(83,$alto, $productoActual -> getNombre(),0,0,"L");
    }else {
        $pdf->Cell(83,$alto, substr($productoActual -> getNombre(), 0, 40) . "...",0,0,"L");
    }
    $pdf->Cell(20,$alto, $productoActual -> getCantidad(),0,0,"C");
    $pdf->Cell(49,$alto, "$ " . $productoActual -> getPrecio(),0,0,"C");
    $pdf->Cell(49,$alto, "$ " . $productoActual -> getPrecio() * $productoActual->getCantidad() ,0,1,"C");
}
$pdf->Cell(196,10,"TOTAL: $ ".number_format($res[2],2),0,1,"R");
$pdf -> Output();
$pdf -> Close();
?>